package com.shumov.tm.repository;

import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.task.TaskException;
import com.shumov.tm.exception.task.TaskIsAlreadyExistException;
import com.shumov.tm.exception.task.TaskListIsEmptyException;
import com.shumov.tm.exception.task.TaskNotExistException;


import java.util.*;

public class TaskRepository {

    private Map<String,Task> taskMap = new LinkedHashMap<>();

    public TaskRepository(){

    }

    public List<Task> findAll() throws TaskException {
        if(taskMap.isEmpty()) {
            throw new TaskListIsEmptyException();
        } else {
            return new ArrayList<>(taskMap.values());
        }
    }

    public Task findOne(String id) throws TaskException {
        if(taskMap.isEmpty()){
            throw new TaskListIsEmptyException();
        } else if(!taskMap.containsKey(id)){
            throw new TaskNotExistException();
        } else {
            return taskMap.get(id);
        }
    }

    public void persist(Task task) throws TaskException {
        if(taskMap.containsKey(task.getId())){
            throw new TaskIsAlreadyExistException();
        } else {
            taskMap.put(task.getId(), task);
        }
    }

    public void merge(String id, Task task) {
        task.setId(id);
        taskMap.put(id, task);
    }

    public void remove(String id) throws TaskException {
        if(taskMap.isEmpty()){
            throw new TaskListIsEmptyException();
        } else if (!taskMap.containsKey(id)){
            throw new TaskNotExistException();
        } else {
            taskMap.remove(id);
        }
    }

    public void removeAll() throws TaskException {
        if (taskMap.isEmpty()){
            throw new TaskListIsEmptyException();
        } else {
            taskMap.clear();
        }

    }
}
