package com.shumov.tm.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Task extends Entity {

    private static List<Task> list = new ArrayList<>();

    private String id = UUID.randomUUID().toString();
    private String name = "Default Name";
    private String description = "Default Description";
    private String idProject = "No Project";
    private Date dateStart;
    private Date dateFinish;

    public Task(String name){
        this.name = name;
    }

    public Task(){

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id){ this.id = id; }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public static List<Task> getList() {
        return list;
    }

}
