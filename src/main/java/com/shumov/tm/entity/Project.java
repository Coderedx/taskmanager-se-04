package com.shumov.tm.entity;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Project extends Entity {

    private static List<Project> list = new ArrayList<>();

    private String id = UUID.randomUUID().toString();
    private String name = "Default Name";
    private String description = "Default Description";
    private Date dateStart;
    private Date dateFinish;

    public Project(String name){
        this.name = name;
    }

    public Project(){

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }



    public static List<Project> getList() {
        return list;
    }



    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id){ this.id = id; }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }
}
