package com.shumov.tm.bootstrap;


import com.shumov.tm.Command;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.input.WrongInputException;
import com.shumov.tm.exception.project.ProjectException;
import com.shumov.tm.exception.task.TaskException;
import com.shumov.tm.repository.ProjectRepository;
import com.shumov.tm.repository.TaskRepository;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;

import java.util.List;
import java.util.Scanner;

public class Bootstrap {

    private ProjectService projectService;
    private TaskService taskService;
    private Scanner scanner = new Scanner(System.in);

    public void init(){
        System.out.println("WELCOME TO TASK MANAGER");
        System.out.println("ENTER \"help\" TO GET COMMAND LIST");
        projectService = new ProjectService(new ProjectRepository());
        taskService = new TaskService(new TaskRepository());
        while (true){
            execute();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void execute() {
        System.out.println("\nENTER COMMAND:");
        Command command = Command.getCommands().get(scanner.nextLine().toLowerCase());
        try {
            commandSwitchSystemStep(command);
            commandSwitchProjectStep(command);
            commandSwitchTaskStep(command);
            commandSwitchFinalStep(command);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

//  Этапы определения команд:
    private void commandSwitchSystemStep(Command command){
        if (command != null){
            switch (command){
                case HELP:
                    this.help();
                    break;
                case EXIT:
                    this.exit();
                    break;
            }
        }
    }

    private void commandSwitchProjectStep(Command command) throws ProjectException, WrongInputException, TaskException {
        if (command != null) {
            switch (command) {
                case CREATE_PROJECT:
                    createProject();
                    break;
                case REMOVE_PROJECT_BY_ID:
                    removeProjectById();
                    break;
                case EDIT_PROJECT_NAME_BY_ID:
                    editProjectNameById();
                    break;
                case PROJECT_LIST:
                    showProjectList();
                    break;
            }
        }
    }

    private void commandSwitchTaskStep(Command command) throws TaskException, ProjectException, WrongInputException {
        if(command != null) {
            switch (command){
                case CREATE_TASK:
                    createTask();
                    break;
                case REMOVE_TASK_BY_ID:
                    removeTaskById();
                    break;
                case EDIT_TASK_NAME_BY_ID:
                    editTaskNameById();
                    break;
                case TASK_LIST:
                    showTaskList();
                    break;
                case ADD_TASK_IN_PROJECT:
                    addTaskInProject();
                    break;
                case REVIEW_PROJECT_TASKS:
                    reviewProjectTasks();
                    break;
            }
        }
    }

    private void commandSwitchFinalStep(Command command){
        if(command == null){
            System.out.println("WRONG COMMAND!");
        }
    }



//  Системные команды:
    private void getAllCommands() {
        for (Command command : Command.values()) {
            System.out.println(command.getCommandText()+": "+command.getDescription());
        }
    }

    private void help() {
        getAllCommands();
    }

    private void exit() {
        System.exit(0);
    }

//  Команды проекта:
    private void createProject() throws ProjectException, WrongInputException {
        // Указать даты
        System.out.println("[PROJECT CREATE]\nENTER PROJECT NAME:");
        String projectNameCreate = scanner.nextLine();
        projectService.isWrongProjectName(projectNameCreate);
        projectService.createProject(projectNameCreate);
        System.out.println("PROJECT HAS BEEN CREATED SUCCESSFULLY");
    }

    private void removeProjectById() throws ProjectException, WrongInputException, TaskException{
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdRemove = scanner.nextLine();
        projectService.isWrongProjectId(projectIdRemove);
        projectService.removeProjectById(taskService, projectIdRemove);
        System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
    }

    private void editProjectNameById() throws ProjectException, WrongInputException {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdEdit = scanner.nextLine();
        projectService.isWrongProjectId(projectIdEdit);
        System.out.println("ENTER NEW PROJECT NAME:");
        String projectNameEdit = scanner.nextLine();
        projectService.isWrongProjectName(projectNameEdit);
        projectService.editProjectNameById(projectIdEdit,projectNameEdit);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }

    private void showProjectList() throws ProjectException{
        System.out.println("[PROJECT LIST]");
        for (Project project : projectService.getProjectList()) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName());
        }
    }

    private void reviewProjectTasks() throws TaskException, ProjectException, WrongInputException {
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdReview = scanner.nextLine();
        projectService.isWrongProjectId(projectIdReview);
        projectService.getProject(projectIdReview);
        showTaskList(taskService.getProjectTasks(projectIdReview));
    }

//  Команды задания:
    private void createTask() throws TaskException, WrongInputException {
        // Указать даты
        System.out.println("[TASK CREATE]\nENTER TASK NAME:");
        String taskNameCreate = scanner.nextLine();
        taskService.isWrongTaskName(taskNameCreate);
        taskService.createTask(taskNameCreate);
        System.out.println("TASK HAS BEEN CREATED SUCCESSFULLY");
    }

    private void removeTaskById() throws TaskException, WrongInputException{
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdRemove = scanner.nextLine();
        taskService.isWrongTaskId(taskIdRemove);
        taskService.removeTaskById(taskIdRemove);
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }

    private void showTaskList() throws TaskException {
        System.out.println("[TASK LIST]");
        for (Task task : taskService.getTaskList()) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }

    private void showTaskList(List<Task> tasks) throws TaskException {
        System.out.println("[TASK LIST]");
        for (Task task : tasks) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }

    private void editTaskNameById() throws TaskException, WrongInputException {
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdEdit = scanner.nextLine();
        taskService.isWrongTaskId(taskIdEdit);
        System.out.println("ENTER NEW TASK NAME:");
        String taskNameEdit = scanner.nextLine();
        taskService.isWrongTaskName(taskNameEdit);
        taskService.editTaskNameById(taskIdEdit,taskNameEdit);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }

    private void addTaskInProject() throws TaskException, ProjectException, WrongInputException{
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdAdd = scanner.nextLine();
        projectService.isWrongProjectId(projectIdAdd);
        projectService.getProject(projectIdAdd);
        System.out.println("ENTER TASK ID:");
        String taskIdAdd = scanner.nextLine();
        taskService.isWrongTaskId(taskIdAdd);
        Task task = taskService.getTask(taskIdAdd);
        task.setIdProject(projectIdAdd);
        taskService.addTaskInProject(taskIdAdd,task);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }

}
