package com.shumov.tm.exception.project;

public class ProjectListIsEmptyException extends ProjectException {

    public ProjectListIsEmptyException() {
        super("PROJECT LIST IS EMPTY!");
    }
}
