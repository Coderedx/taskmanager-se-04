package com.shumov.tm.exception.project;

public class ProjectNotExistException extends ProjectException {

    public ProjectNotExistException(){
        super("PROJECT WITH THIS ID DOES NOT EXIST!");
    }

}
