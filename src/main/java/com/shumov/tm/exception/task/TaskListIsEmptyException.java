package com.shumov.tm.exception.task;

public class TaskListIsEmptyException extends TaskException {

    public TaskListIsEmptyException(){
        super("TASK LIST IS EMPTY!");
    }
}
