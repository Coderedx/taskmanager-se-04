package com.shumov.tm.exception.task;

public class TaskNotExistException extends TaskException {

    public TaskNotExistException(){
        super("TASK WITH THIS ID DOES NOT EXIST!");
    }
}
